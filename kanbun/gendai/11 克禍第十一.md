# 克禍第十一

かつて上蔡（李斯）は処刑の前に黄色い犬を懐かしみ、杜陵（杜甫）は雲が蒼い（灰色）犬に変わるのを惜しみました。（このように世ははかないもので、）趙括の母は子を弾劾して連座を免れ、張俶の父は同じことをしてなお殺されました。宣子（智申）は瑤を跡継ぎにして智の家を滅ぼし、輔果は名字を変えてこの難を逃れました。北海（孔融）が曹操を侮辱して死刑となった時、二人の子どもは遊戯をしながらこれを待ちました。運命は度し難く、忘れた頃に災難を降らせます。人がこれに向き合う時に、その生き様がありありと表れます。あるいは（『捜神記』の言い伝えに）麋子仲（糜竺）が不思議な女性の好意で、家が燃えることを知ったとか、諸葛恪が暗殺される前に、犬と血の匂いの予兆があったとか、君子たちが社宮で密談する夢で曹の国が滅んだとか、池陽宮で遊ぶ小人たちの影が見えて王莽が失脚したとか、永嘉（年間）に犬がしゃべって天下が飢え、太安（年間）に牛がしゃべって天下が乱れたなどと言いますが、予知能力などない我々凡人は、何とか台風が来る前に窓を打ち付け、炎上する前に火の粉を遠ざけ、携帯をなくす前にストラップをつけ、黒塗りの高級車に追突しないよう前を見るしかありません。それで『韓非子』にも「物事の異常にも（病気のように）必ず初期症状があり、賢い人は早めに手を打つものだ」と言うのです。

世の中のことばかりでなく、**程式**〔プログラム〕も同じです。程式を**運行**〔実行〕していると、時に作者の不注意のために禍根（バグ）が混入していたり、**機器**〔マシン〕の不具合で内部から**禍**〔エラー〕が生じたり、ユーザーの勘違いで外部から禍が生じたり、あるいは不可抗力によって予想外の禍が発生したりします。その結果が軽ければちょっと時間をふいにするだけで済みますが、重ければ経済に大損害を与えかねません。伝え聞くところでは、かつて**編程**〔プログラミング〕の初期の頃に、ある先人がいつも間違いを起こす程式に遭遇し、機器を開けてみればなんのことはない、蛾が 1 匹入っていたとのこと。その恨みを晴らすため、死体は王莽の首（さらし首）のごとく紙にテープで貼り付けられ、今も残っているといいます。

程式での**克禍**〔例外処理〕のやり方は、まず失敗して禍を出す恐れのある部分の前に、すべて姑妄〔ダメ元〕である旨を書いておき、後ろにその考えられる原因と解法〔対処法〕を列挙し、**補救**〔回復処理〕とするものです。よく見られる禍は 4 つあり、**虛指**〔参照エラー〕、**文法**〔構文エラー〕、**異類**〔型エラー〕、**逾界**〔境界エラー〕です。これらを原因とともに例示します。

```
姑妄行此。
	注曰。「「行險著於是。」」

如事不諧。豈「「虛指」」之禍歟。
	吾有一言。曰「「本無此物。奈何用之」」。書之。

豈「「文法」」之禍歟。
	吾有一言。曰「「不合文法。不通之甚」」。書之。

豈「「異類」」之禍歟。
	吾有一言。曰「「物各其類。豈可混同」」。書之。
	
豈「「逾界」」之禍歟。
	吾有一言。曰「「祭不越望。俎不代庖」」。書之。

不知何禍歟。
	吾有一言。曰「「嗚呼哀哉。伏維尚飨」」。書之。
乃作罷。
```

もし姑妄の行で禍が発生しなければ、解法は実行されません。もし発生すれば、直ちに解法を見て一致するものがあれば実行し、他のものは実行せず、直接「乃作罷」の後に移動します。解法の最後に、「不知何禍歟」を加えて、予想外の事態に備えることもできます。これがないと、解法が見つけられなかった時にそのまま**崩潰**〔クラッシュ〕します。

また、**報錯**〔例外を投げる〕の手段もあり、自分で禍を定義して発動することができます。  
**Q**: そのりくつはおかしい。禍が起きなければ諸手を挙げて喜ぶものじゃないですか？　なんでわざわざ禍を起こすんですか？  
**A**: まず 1 つに、危なそうな兆候が見えていたら、ただ失敗するのを待つよりも、取り返しのつくうちに警報を出し、克禍の法で補救するほうがいいからです。2 つめに、程式は大人数で協力して組むことも多く、それぞれが分担したり引用しあったりします。そのため（例外を）知らせる人と対処する人は同じとは限らないし、使う（※ライブラリなどとして取り込むことを言っているか）人に至っては誰ともわかりません。だから私が例外を発するのは、他の人にこうしてはいけないと知らせるためです。どう対処するかは、相手の仕事です。  
報錯は次のように行います。

```
嗚呼。「「滅頂」」之禍。
```

「滅頂」というのは、禍の名前であり、禍の原因を考慮して決めるものです。あらかじめ用意されている「虛指」「文法」「異類」「逾界」を選ぶこともできます。発生した後は、先ほどと同様に解法を書きます。

```
姑妄行此。
	嗚呼。「「滅頂」」之禍。
如事不諧。豈「「滅頂」」之禍歟。
	吾有一言。曰「「嗚呼哀哉。伏維尚飨」」。書之。
乃作罷。
```

また、禍の後ろに**言**〔文字列〕を付け加えて、禍の詳しい情報を示すことができます。これは禍の名称は同じでも、発生した背景が異なり、説明が必要になることがあるためです。禍の「**文**〔メッセージ〕」とも言います。例は次の通り。

```
嗚呼。「「無關」」之禍。曰「「事不關心。關心者亂」」。
嗚呼。「「自取」」之禍。曰「「招禍取咎。無不自己」」。
```

**Q**: 解法の中ではどうやってこれを取り出すのですか？  
**A**: その禍を、次のように**物**〔オブジェクト〕として名づけます。「名」「文」はすでに説明した通りですが、「**蹤**〔トレース〕」というのは、禍の発生源を記述して、検索に役立てるためのものです。

```
姑妄行此。
	嗚呼。「「無關」」之禍。曰「「事不關心。關心者亂」」。
如事不諧。豈「「無關」」之禍歟。名之曰「禍」。
	夫「禍」之「「名」」。書之。
	夫「禍」之「「文」」。書之。
	夫「禍」之「「蹤」」。書之。
不知何禍歟。名之曰「奇禍」。
	夫「奇禍」之「「名」」。書之。
乃作罷。
```

ある**數**〔数値〕を 0 で**除**す〔割る〕と、**商**はいくつか。これを例にとってみます。

```
吾有一術。名之曰「除」。欲行是術。必先得二數。曰「甲」。曰「乙」。乃行是術曰。
	若「乙」等於零者。
		嗚呼。「「零除」」之禍。曰「「除數不得為零」」。
	云云。
	除「甲」以「乙」。乃得矣。
是謂「除」之術也。

施「除」於二。於三。書之。
施「除」於四。於零。書之。
```

**Q**: いやいや、零除〔ゼロ除算〕は確かにいけないですが、無窮〔無限〕に漸近するわけなので、問答無用で禍にするのはいかがなものか。  
**A**: では克禍の法をやや緩めて、便宜的に數の最大値を取って脱出させることにします。0 ÷ 0 はさすがにどうしようもないので、解法の中でもう一度報錯して最終結果とします。

```
吾有一術。名之曰「除乙」。欲行是術。必先得二數。曰「甲」。曰「乙」。乃行是術曰。
	姑妄行此。
		施「除」於「甲」。於「乙」。乃得矣。
	如事不諧。豈「「零除」」之禍歟。
		若「甲」大於零者。
			乃得一載。
		或若「甲」小於零者。
			乃得負一載。
		若非。
			嗚呼。「「零除零」」之禍。曰「「二數皆零。無可救也」」。
		云云。
	乃作罷。
是謂「除乙」之術也。
```

**Q**: 0 ÷ 0 も本当はいけないんですが、今はどうしても禍を出したくないので、強引に 0 にしちゃってもいいですか？  
**A**: では……

```
吾有一術。名之曰「除丙」。欲行是術。必先得二數。曰「甲」。曰「乙」。乃行是術曰。
	姑妄行此。
		施「除乙」於「甲」。於「乙」。乃得矣。
	如事不諧。豈「「零除零」」之禍歟。
		乃得零。注曰「「氣殺算學先生也」」。
	乃作罷。
是謂「除丙」之術也。
```

今度は、前の章にあった荘子の棰術を報錯を使って書き換え、不適当な**參**〔引数〕を禁止するようにします。

```
吾有一術。名之曰「量棰」。欲行是術。必先得二數。曰「今長」。曰「餘日」。乃行是術曰。
	若「今長」不大於零者。嗚呼。「「參數」」之禍。曰「「萬世不竭。必當為正」」也。
	若「餘日」小於零者。　嗚呼。「「參數」」之禍。曰「「逝者如斯。豈得逆耶」」也。
	
	若「餘日」等於零者。乃得「今長」也。
	除「今長」以二。昔之「今長」者。今其是矣。
	減「餘日」以一。昔之「餘日」者。今其是矣。
	施「量棰」於「今長」於「餘日」。乃得矣。
是謂「量棰」之術也。
```

このように、**術**〔関数〕を走らせる前に參數をチェックすることを、「**斷言**〔ガード〕」とも言います。「斷言」は、いわば城を囲む堀、歯を守る唇であり、自分への戒めとしても、他者への警鐘としても有益です。できる人はみんな使っています。

克禍の道、これで完全に理解しましたね。  
**Q**: 実に素晴らしい！　これからは私の程式を「姑妄行此」から始め、「不知何禍歟」で閉じることにします。これでどんなエラーが起きても全部吸収してくれて、永久に禍とおさらばです。やったね！  
**A**: それはヤバいし、アンチパターンです。小さな禍を握りつぶせば、いずれ大きな禍を招きます。ダメ、ゼッタイ。
